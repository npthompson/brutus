Get a string that is encrypted
convert the string to lower case
store our offset
keep a list of fairly common three and four letter words to try and match

test the starting offset:
  create a duplicate string of the original encrypted string.
  adjust all the letters per the current offset
  check for matches of 3 and 4 letter words. Optional: count matches, use it to track a score.
  store the possible matches, with the offset used to achieve it, and the score if that was recorded.
  increase the offset index
  continue the loop
 
show the possible matches to the user.
from typing import Union
from dataclasses import dataclass, field


@dataclass
class Result:
    original_phrase: str
    phrase: str
    match_score: int
    shift: int = field(default = None)

    def __str__(self):
        return f"{self.phrase}\nMatch Score: {self.match_score}\tShift: {self.shift}"

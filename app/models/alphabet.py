import string
import unittest
from unittest.case import TestCase
from collections import Counter

class Alphabet:

    ALPHABET = string.ascii_letters[:26]
    PUNCTUATION = string.punctuation

    def __init__(self, index = 0, alphabet = ALPHABET):
        self.index = index
        self.alphabet = list(alphabet)
        self.memory = dict()

    
    def translate(self, string) -> str:
        string = string.lower()
        old_letters = list(string)
        new_letters = []

        for letter in old_letters:
            if letter in self.alphabet:
                new_letter_index = (self.alphabet.index(letter) + self.index) % len(self.alphabet)
                new_letters.append(self.alphabet[new_letter_index])
            else:
                new_letters.append(letter)
        
        return "".join(new_letters)


    def count_instances(self, word, phrase):
        '''count the occurrences of a word'''
        phrase = phrase.lower()
        phrase = phrase.replace("\r","")
        phrase = phrase.replace("\n", " ")
        for punc in list(self.PUNCTUATION):
            phrase = phrase.strip(punc)

        if phrase in self.memory.keys():
            word_counter = self.memory[phrase]
        else:
            word_counter = Counter(phrase.split(" "))
            self.memory[phrase] = word_counter

        if word in word_counter.keys():
            return word_counter[word]
        else:
            return 0


class Tests(TestCase):

    def test_rot13(self):
        original = "Hello World!"
        cyphered = "uryyb jbeyq!"

        a = Alphabet(13)
        self.assertEquals(a.translate(original), cyphered)
        self.assertEquals(a.translate(cyphered), original.lower())


    def test_count_word_instances(self):
        original = "The quick brown fox jumped over the lazy dog."
        a = Alphabet(0)
        self.assertEquals(a.count_instances("the",original),2)
        self.assertEquals(a.count_instances('fox',original),1)
        self.assertEquals(a.count_instances("dog.", original), 1)
        self.assertEquals(a.count_instances("bigfoot", original), 0)


if __name__ == "__main__":
    unittest.main()

import json
from pathlib import Path

class WordList:

    def __init__(self, filename = "words.json"):
        self.wordfile = Path.joinpath(Path.cwd(), filename)
        if not self.wordfile.exists(): raise ValueError(f"Cannot open {filename}, it doesn't seem to exist.")

        self.words = json.loads(self.wordfile.read_text(encoding="utf-8"))


    def save_list(self, filename = None):
        if filename == None:
            save_path = str(self.wordfile)
        else:
            save_path = filename
        
        with open(save_path,'w', encoding = "utf-8") as fh:
            json.dump(self.words,fh)


    def add_word(self,word):
        word = word.lower()
        if not word in self.words and word.isalpha():
            self.words.append(word)
            return True
        return False
    

    def remove_word(self,word):
        word = word.lower()
        if word in self.words:
            self.words.remove(word)
            return True
        return False
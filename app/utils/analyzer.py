from typing import List
import unittest
from unittest import TestCase

from models.alphabet import Alphabet
from models.result import Result
from models.wordlist import WordList


class Analyzer:

    @staticmethod   
    def attack(phrase, wordlist: WordList) -> List[Result]:
        results = []
        for offset in range(0,26):
            alphabet = Alphabet(offset)
            translation = alphabet.translate(phrase)
            match_score = 0
            for word in wordlist.words:
                match_score += alphabet.count_instances(word,translation)
            
            if match_score == 0:
                continue
            else:
                results.append(Result(phrase,translation,match_score,len(alphabet.alphabet) - offset))
        
        return results


class TestAnalyzer(TestCase):

    def test_basic_use(self):
        wl = WordList()
        phrase = "gur dhvpx oebja sbk whzcrq bire gur ynml qbt"
        decoded = "the quick brown fox jumped over the lazy dog"
        results = Analyzer.attack(phrase,wl)
        best_result = list(sorted(results,key = lambda x: x.match_score, reverse=True))[0]
        self.assertEqual(best_result.original_phrase, phrase)
        self.assertEqual(best_result.phrase, decoded)


if __name__ == "__main__":
    unittest.main()
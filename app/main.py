from cmd import Cmd

from utils.analyzer import Analyzer
from models.wordlist import WordList

from colorama import Fore,init as colorama_init

colorama_init(autoreset=True)

class Application(Cmd):
    intro = "Brutus: for attacking the Ceasar cypher.\ntype help for commands."

    def __init__(self):
        super().__init__()
        self.wordlist = WordList()


    def do_quit(self,_):
        '''end the program'''
        exit(0)


    do_exit = do_quit


    def do_line(self, line:str):
        '''crack one line of input'''
        
        results = Analyzer.attack(line,self.wordlist)
        if not results:
            print(f"\n{Fore.LIGHTYELLOW_EX}Unable to decipher.\n")
            return
        
        results = sorted(results,key = lambda x: x.match_score, reverse = True)
        results = results[:5]
        
        for result in results:
            print(f"\n{Fore.LIGHTGREEN_EX}{result.phrase}")
            print(f"Match score: {result.match_score}\tShift: {result.shift}")
        
        print() # spacer


    def do_words(self,argument):
        '''list, add, remove, or save the word list.'''
        argument = argument.lower().strip()

        if argument == "list":
            print(*self.wordlist.words,sep=", ")
            print() # spacer
        
        elif argument == "add":
            new_word = input("Add what word? ").lower().strip()
            if new_word.isalpha():
                if self.wordlist.add_word(new_word):
                    print(f"{Fore.LIGHTGREEN_EX}Added {new_word} to the word list.")
                else:
                    print(f"{Fore.LIGHTRED_EX}Unable to add the word {new_word} to the list.")
            else:
                print(f"Invalid input: {new_word}")
            print() # spacer
        
        elif argument == "remove":
            remove_word = input("Remove what word? ").lower().strip()
            if self.wordlist.remove_word(remove_word):
                print(f"{Fore.LIGHTGREEN_EX}Removed {remove_word} from the word list.")
            else:
                print(f"{Fore.LIGHTRED_EX}Unable to remove the word {remove_word} from the list.")
            print() #spacer
        
        elif argument == "save":
            self.wordlist.save_list()
            print(f"{Fore.LIGHTGREEN_EX}Saved the word list")
            print() # spacer
        
        else:
            print("Please specify if you wish to {0}list{1}, {0}add{1} to, {0}remove{1} from, or {0}save{1} the word list".format(Fore.LIGHTYELLOW_EX, Fore.RESET))
            print() # spacer


    def do_cls(self,_):
        '''clear the screen'''
        print("\033[2J\033[H")

    do_clear = do_cls

if __name__ == "__main__":
    Application().cmdloop()